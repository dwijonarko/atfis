## ATFIS - Attendance Fingerprint Information System (FrontEnd)

Atfis merupakan sistem informasi yang digunakan untuk menghubungkan perangkat kehadiran dengan sidikjari / fingerprint dengan aplikasi web sebagai antar muka (*menggunakan connector terpisah*). Dengan aplikasi ini, pengguna dapat melihat daftar kehadiran diri sendiri perhari, perbulan dan pertahun, kemudian mampu mengisi kehadiran melalui aplikasi (tidak harus dari fingerprint jika diperlukan)

Dalam aplikasi ini terdapat notifikasi jika belum melakukan pengisian daftar hadir baik untuk berangkat maupun ketika akan pulang.

## Dependensi

Dibangun menggunakan framework [Laravel ](http://laravel.com/).
Untuk tampilan menggunakan [Twitter Bootstrap] (http://getbootstrap.com), tampilan table menggunakan library [Datatables] (http://datatables.net) dan didukung dengan [JQuery](http://jquery.com)

## Development
Masih dalam versi beta pengembangan

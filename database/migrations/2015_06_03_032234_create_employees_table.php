<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nik','24')->unique();
			$table->string('name');
			$table->string('place_of_birth');
			$table->date('birthday');
			$table->string('address');
			$table->string('phone');
			$table->string('email')->unique();
			$table->integer('position_id')->unsigned();
			$table->foreign('position_id')->references('id')->on('positions');
			$table->integer('office_id')->unsigned();
			$table->foreign('office_id')->references('id')->on('offices');
			$table->integer('schedule_id')->unsigned();
			$table->foreign('schedule_id')->references('id')->on('schedules');
			$table->tinyInteger('active');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}

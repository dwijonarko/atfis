<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->username='administrator';
        $user->email = 'administrator@atfis.dev';
        $user->password = Hash::make('administrator');
        $user->realname="Atfis administrator";
        $user->save();

        $user = \App\User::where('username', 'administrator')->first();

	  $role = \App\Role::where('name','administrator')->first();
	  $user->assignRole($role);

    }
}

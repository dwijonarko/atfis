<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\Role();
        $administrator->name='administrator';
        $administrator->save();

        $user = new \App\Role();
        $user->name='user';
        $user->save();
    }
}

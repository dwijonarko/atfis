<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Position;
use App\Office;
use App\Schedule;

class Employee extends Model {

	protected $fillable = [];

  public function position(){
    return $this->belongsTo('App\Position');
  }

  public function office(){
    return $this->belongsTo('App\Office');
  }

  public function schedule(){
    return $this->belongsTo('App\Schedule');
  }

  public function attendances(){
    return $this->hasMany('App\Attendance');
  }

}

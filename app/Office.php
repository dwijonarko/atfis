<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Office extends Model {

	public function employees(){
    return $this->hasMany('Employee');
  }

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Office;
use Datatables;

class OfficeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('office/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('office/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
 			'name' => 'required|unique:offices',
 			'address'=>'required',
 			'phone'=>'required'
		]);
		

		if ($v->passes()) {
			$office     = new Office();
			$office->name = $request->input('name');
			$office->address=$request->input('address');
			$office->phone=$request->input('phone');
			$office->created_at = date('Y-m-d H:i:s');

			if($office->save())
			{            
	 		 $message = array(array("message"=>"Data was saved"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$office = Office::find($id);

		return view('office/edit')
			->with('office', $office);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$v = Validator::make($request->all(), [
 			'name' => 'required',
 			'address'=>'required',
 			'phone'=>'required'
		]);
		

		if ($v->passes()) {
			$office     = Office::find($request->input('id'));
			$office->name = $request->input('name');
			$office->address=$request->input('address');
			$office->phone=$request->input('phone');
			$office->updated_at = date('Y-m-d H:i:s');

			if($office->save())
			{            
	 		 $message = array(array("message"=>"Data was update"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$office = Office::find($id);
		if($office->delete())
			{            
	 		 $message = array(array("message"=>"Data was deleted"));
	 		 return json_encode($message);
      }
	}

	 public function getData()
    {
        $offices = Office::all();

        return Datatables::of($offices)
        ->addColumn('operations','<a href="#" url="{{ URL::route( \'office.edit\', array( $id )) }}"class=\'btn btn-xs btn-info btn-edit\' ><i class=\'fa fa-edit\'></i>Edit</a> &nbsp; 
        	<a data="#" url="{{ URL::route( \'office.destroy\', array( $id )) }} "class=\'btn btn-xs btn-danger btn-destroy\' id=\'{{$id}}\' _token={{csrf_token()}}><i class=\'fa fa-close\'></i>Delete</a>')
        ->make(true);
    }

}

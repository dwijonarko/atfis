<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ReportsController extends Controller {

	/**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function dailyAll(){
    $reports = \DB::table('attendances')
      ->select(\DB::raw('employees.nik,employee_id,date,MIN(CASE WHEN in_out=0 THEN DATE_FORMAT(time,\'%H:%i:%s\') END) masuk, MAX(CASE WHEN in_out=1 THEN DATE_FORMAT(time,\'%H:%i:%s\') END) keluar '))
      ->join('employees','attendances.employee_id','=','employees.id')
       ->where('date' ,date('Y:m:d'))
      ->groupBy('employee_id')
      ->get();
    return view('reports/dailyAll')->with(array('reports'=>$reports));
  }

  public function monthlyAll(){
    //cari pertama kali absen masuk dan cari terakhir absen keluar berdasarkan tanggal
    $reports = \DB::table('attendances')
      ->select(\DB::raw('employees.nik,employee_id,date AS tanggal,(CASE WHEN `in_out`=0 THEN time ELSE (SELECT min(time) FROM attendances WHERE in_out=\'0\' AND date=tanggal) END) masuk,(CASE WHEN `in_out`=\'1\' THEN time ELSE (SELECT max(time) FROM attendances WHERE in_out=\'1\' AND date=tanggal) END) keluar'))
      ->join('employees','attendances.employee_id','=','employees.id')
      ->where(\DB::raw('month(date)'),date('m'))
      ->where(\DB::raw('year(date)'),date('Y'))
      ->groupBy('tanggals')
      ->get();
    return view('reports/monthlyAll')->with(array('reports'=>$reports));
  }

  public function annualyAll(){
    //cari pertama kali absen masuk dan cari terakhir absen keluar berdasarkan tanggal
    $reports = \DB::table('attendances')
      ->select(\DB::raw('employees.nik,employee_id,date AS tanggal,(CASE WHEN `in_out`=0 THEN time ELSE (SELECT min(time) FROM attendances WHERE in_out=\'0\' AND date=tanggal) END) masuk,(CASE WHEN `in_out`=\'1\' THEN time ELSE (SELECT max(time) FROM attendances WHERE in_out=\'1\' AND date=tanggal) END) keluar'))
      ->join('employees','attendances.employee_id','=','employees.id')
      ->where(\DB::raw('year(date)'),date('Y'))
      ->groupBy('tanggal')
      ->get();
    return view('reports/annualyAll')->with(array('reports'=>$reports));
  }

  public function daily(){
    $reports = \DB::table('attendances')
      ->select(\DB::raw('employees.nik,employee_id,date,MIN(CASE WHEN in_out=0 THEN DATE_FORMAT(time,\'%H:%i:%s\') END) masuk, MAX(CASE WHEN in_out=1 THEN DATE_FORMAT(time,\'%H:%i:%s\') END) keluar '))
      ->join('employees','attendances.employee_id','=','employees.id')
       ->where('date' ,date('Y:m:d'))
       ->where('employees.nik',\Auth::user()->username)
      ->groupBy('employee_id')
      ->get();
    return view('reports/dailyAll')->with(array('reports'=>$reports));
  }

  public function monthly(){
    //cari pertama kali absen masuk dan cari terakhir absen keluar berdasarkan tanggal
    $reports = \DB::table('attendances')
      ->select(\DB::raw('employees.nik,employee_id,date AS tanggal,(CASE WHEN `in_out`=0 THEN time ELSE (SELECT min(time) FROM attendances WHERE in_out=\'0\' AND date=tanggal) END) masuk,(CASE WHEN `in_out`=\'1\' THEN time ELSE (SELECT max(time) FROM attendances WHERE in_out=\'1\' AND date=tanggal) END) keluar'))
      ->join('employees','attendances.employee_id','=','employees.id')
      ->where(\DB::raw('month(date)'),date('m'))
      ->where(\DB::raw('year(date)'),date('Y'))
      ->where('employees.nik',\Auth::user()->username)
      ->groupBy('tanggal')
      ->get();
    return view('reports/monthlyAll')->with(array('reports'=>$reports));
  }

  public function annualy(){
    //cari pertama kali absen masuk dan cari terakhir absen keluar berdasarkan tanggal
    $reports = \DB::table('attendances')
      ->select(\DB::raw('employees.nik,employee_id,date AS tanggal,(CASE WHEN `in_out`=0 THEN time ELSE (SELECT min(time) FROM attendances WHERE in_out=\'0\' AND date=tanggal) END) masuk,(CASE WHEN `in_out`=\'1\' THEN time ELSE (SELECT max(time) FROM attendances WHERE in_out=\'1\' AND date=tanggal) END) keluar'))
      ->join('employees','attendances.employee_id','=','employees.id')
      ->where(\DB::raw('year(date)'),date('Y'))
      ->where('employees.nik',\Auth::user()->username)
      ->groupBy('tanggal')
      ->get();
    return view('reports/annualyAll')->with(array('reports'=>$reports));
  }


}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Schedule;
use Datatables;

class ScheduleController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('schedule/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schedule/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
 			'code' => 'required|unique:schedules',
 			'in_time'=>'required',
 			'out_time'=>'required'
		]);
		

		if ($v->passes()) {
			$schedule     = new Schedule();
			$schedule->code = $request->input('code');
			$schedule->day = $request->input('day');
			$schedule->in_time = $request->input('in_time');
			$schedule->out_time = $request->input('out_time');
			$schedule->created_at = date('Y-m-d H:i:s');

			if($schedule->save())
			{            
	 		 $message = array(array("message"=>"Data was saved"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$schedule = Schedule::find($id);

		return view('schedule/edit')
			->with('schedule', $schedule);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$v = Validator::make($request->all(), [
 			'code' => 'required',
 			'in_time'=>'required',
 			'out_time'=>'required'
		]);
		

		if ($v->passes()) {
			$schedule     = Schedule::find($request->input('id'));
			$schedule->code = $request->input('code');
			$schedule->day = $request->input('day');
			$schedule->in_time = $request->input('in_time');
			$schedule->out_time = $request->input('out_time');
			$schedule->created_at = date('Y-m-d H:i:s');

			if($schedule->save())
			{            
	 		 $message = array(array("message"=>"Data was update"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$schedule = Schedule::find($id);
		if($schedule->delete())
			{            
	 		 $message = array(array("message"=>"Data was deleted"));
	 		 return json_encode($message);
      }
	}

	 public function getData()
    {
        $schedules = Schedule::all();

        return Datatables::of($schedules)
        ->addColumn('operations','<a href="#" url="{{ URL::route( \'schedule.edit\', array( $id )) }}"class=\'btn btn-xs btn-info btn-edit\' ><i class=\'fa fa-edit\'></i>Edit</a> &nbsp; 
        	<a data="#" url="{{ URL::route( \'schedule.destroy\', array( $id )) }} "class=\'btn btn-xs btn-danger btn-destroy\' id=\'{{$id}}\' _token={{csrf_token()}}><i class=\'fa fa-close\'></i>Delete</a>')
        ->make(true);
    }

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Attendance;
use App\Employee;

class AttendanceController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$employee = Employee::where('nik',\Auth::user()->username)->first(array('employees.id'));
		$attendance = new Attendance();
		$attendance->date = date('Y-m-d');
		$attendance->time = date('H:i:s');
		$attendance->employee_id = $employee->id;
		$attendance->in_out=$request->input('id');
		if($attendance->save())
		{          
			$attendances = Attendance::where('employee_id',$employee->id)->where('date',date('Y-m-d'))->take(5)->orderBy('time','desc')->get();
			$message = array(array("message"=>"Data was saved", "attendances"=>$attendances));
			return json_encode($message);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

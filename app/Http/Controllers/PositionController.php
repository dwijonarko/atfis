<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Position;
use Datatables;

class PositionController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('position/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('position/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
 			'name' => 'required|unique:positions'
		]);
		

		if ($v->passes()) {
			$position     = new Position();
			$position->name = Input::get('name');
			$position->created_at = date('Y-m-d H:i:s');

			if($position->save())
			{            
	 		 $message = array(array("message"=>"Data was saved"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$position = Position::find($id);

		return view('position/edit')
			->with('position', $position);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$v = Validator::make($request->all(), [
 			'name' => 'required|unique:positions'
		]);
		

		if ($v->passes()) {
			$position     = Position::find(Input::get('id'));
			$position->name = Input::get('name');
			$position->updated_at = date('Y-m-d H:i:s');

			if($position->save())
			{            
	 		 $message = array(array("message"=>"Data was update"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$position = Position::find($id);
		if($position->delete())
			{            
	 		 $message = array(array("message"=>"Data was deleted"));
	 		 return json_encode($message);
      }
	}

	 public function getData()
    {
        $positions = Position::all();

        return Datatables::of($positions)
        ->addColumn('operations','<a href="#" url="{{ URL::route( \'position.edit\', array( $id )) }}"class=\'btn btn-xs btn-info btn-edit\' ><i class=\'fa fa-edit\'></i>Edit</a> &nbsp; 
        	<a data="#" url="{{ URL::route( \'position.destroy\', array( $id )) }} "class=\'btn btn-xs btn-danger btn-destroy\' id=\'{{$id}}\' _token={{csrf_token()}}><i class=\'fa fa-close\'></i>Delete</a>')
        ->make(true);
    }

}

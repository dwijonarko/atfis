<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Employee;
use App\Position;
use App\Office;
use App\Schedule;
use App\User;
use App\Role;
use Datatables;

class EmployeeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('employee/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$positions = Position::select(['id','name'])->get();
		$offices = Office::select(['id','name'])->get();
		$schedules = Schedule::select(['id','code'])->get();
		return view('employee/create',array('positions'=>$positions,'offices'=>$offices,'schedules'=>$schedules));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$messages = [
    'nik.required' => 'Kolom Nomor Induk harus di isi.',
    'name.required' => 'Kolom Nama harus di isi.',
    'place_of_birth.required' => 'Kolom Tempat Lahir harus di isi.',
    'birthday.required' => 'Kolom Tanggal Lahir harus di isi.',
    'address.required' => 'Kolom Alamat harus di isi.',
    'phone.required' => 'Kolom Nomor Telepon harus di isi.',
    'email.required' => 'Kolom Email harus di isi.',
    'position_id.required' => 'Kolom Jabatan harus di pilih.',
    'office_id.required' => 'Kolom Kantor harus di pilih.',
    'schedule_id.required' => 'Kolom Jadwal harus di pilih.',
		];
		$v = Validator::make($request->all(), [
 			'nik' => 'required|unique:employees',
 			'name'=>'required',
 			'place_of_birth'=>'required',
 			'birthday'=>'required',
 			'address'=>'required',
 			'phone'=>'required',
 			'email'=>'required|email|unique:employees',
			'position_id'=>'required|digits_between:1,100',
			'office_id'=>'required|digits_between:1,100',
			'schedule_id'=>'required|digits_between:1,100',
		],$messages);

		if ($v->passes()) {
			$employee     = new Employee();
			$employee->nik = $request->input('nik');
			$employee->name = $request->input('name');
			$employee->place_of_birth = $request->input('place_of_birth');
			$employee->birthday = \DateTime::createFromFormat('j:m:Y',$request->input('birthday'));
			$employee->address = $request->input('address');
			$employee->phone = $request->input('phone');
			$employee->email = $request->input('email');
			$employee->position_id = $request->input('position_id');
			$employee->office_id = $request->input('office_id');
			$employee->schedule_id = $request->input('schedule_id');
			if (!$request->input('active')) {
				$employee->active=0;
			}else{
				$employee->active = $request->input('active');	
			}
			$employee->created_at = date('Y-m-d H:i:s');

			if($request->input('register')){
					$user = new User();
					$user->username = $request->input('nik');
					$user->realname = $request->input('name');
					$user->email = $request->input('email');
					$user->password =  \Hash::make($request->input('nik'));

					if($employee->save() && $user->save())
					{
						$role = Role::where('name','user')->first();
						$user->assignRole($role);            
						$message = array(array("message"=>"Data was saved"));
						return json_encode($message);
					}
			}else{
				if($employee->save())
				{            
					$message = array(array("message"=>"Data was saved"));
					return json_encode($message);
				}
			}

 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$employee = Employee::find($id);

		return view('employee.show')
			->with('employee', $employee);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$employee = Employee::find($id);
		$positions = Position::select(['id','name'])->get();
		$offices = Office::select(['id','name'])->get();
		$schedules = Schedule::select(['id','code'])->get();
		return view('employee/edit')
			->with('employee', $employee)
			->with('positions', $positions)
			->with('offices', $offices)
			->with('schedules', $schedules);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$messages = [
    'name.required' => 'Kolom Nama harus di isi.',
    'place_of_birth.required' => 'Kolom Tempat Lahir harus di isi.',
    'birthday.required' => 'Kolom Tanggal Lahir harus di isi.',
    'address.required' => 'Kolom Alamat harus di isi.',
    'phone.required' => 'Kolom Nomor Telepon harus di isi.',
    'email.required' => 'Kolom Email harus di isi.',
    'position_id.required' => 'Kolom Jabatan harus di pilih.',
    'office_id.required' => 'Kolom Kantor harus di pilih.',
    'schedule_id.required' => 'Kolom Jadwal harus di pilih.',
		];
		$v = Validator::make($request->all(), [
 			'name'=>'required',
 			'place_of_birth'=>'required',
 			'birthday'=>'required',
 			'address'=>'required',
 			'phone'=>'required',
 			'email'=>'required|email',
			'position_id'=>'required|digits_between:1,100',
			'office_id'=>'required|digits_between:1,100',
			'schedule_id'=>'required|digits_between:1,100',
		],$messages);

		if ($v->passes()) {
			$employee     = Employee::find($request->input('id'));
			$employee->nik = $request->input('nik');
			$employee->name = $request->input('name');
			$employee->place_of_birth = $request->input('place_of_birth');
			$employee->birthday = \DateTime::createFromFormat('j:m:Y',$request->input('birthday'));
			$employee->address = $request->input('address');
			$employee->phone = $request->input('phone');
			$employee->email = $request->input('email');
			$employee->position_id = $request->input('position_id');
			$employee->office_id = $request->input('office_id');
			$employee->schedule_id = $request->input('schedule_id');
			if (!$request->input('active')) {
				$employee->active=0;
			}else{
				$employee->active = $request->input('active');	
			}
			$employee->updated_at = date('Y-m-d H:i:s');

			if($employee->save())
			{            
	 		 $message = array(array("message"=>"Data was saved"));
	 		 return json_encode($message);
      }
 		}else{
 			$message = json_encode( $v->messages());
		 	return $message;
 		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$employee = Employee::find($id);
		$name = $employee->nik;
		$user = User::where('username','=',$name)->first();
		if ($user) {
			if($employee->delete() && $user->delete() ){
				$message = array(array("message"=>"Data was deleted"));
				return json_encode($message);
			}else{
				$message = array(array("message"=>"Ouups error was found"));
				return json_encode($message);
			}
		}else{
			if($employee->delete()){
				$message = array(array("message"=>"Data was deleted"));
				return json_encode($message);
			}
		}

	}

	 public function getData()
    {
        // $employees = Employee::all();
    	$employees = Employee::join('positions', 'employees.position_id', '=', 'positions.id')
    				->join('offices', 'employees.office_id', '=', 'offices.id')
    				->join('schedules', 'employees.schedule_id', '=', 'schedules.id')
            ->select(['employees.id','employees.nik','employees.name','employees.active AS active','positions.name AS position', 'offices.name AS office','schedules.code AS schedule']);
        return Datatables::of($employees)
        ->editColumn('nik', function ($employee) {
                return '<a href="#" url="employee/'.$employee->id.'"class=\'btn-show\' >'.$employee->nik.'</a>';
            })
        ->editColumn('active', function ($employee) {
								$active=($employee->active ==0) ? '<span class=\'label label-danger\'>Not Active</span>' : '<span class=\'label label-success\'>Active</span>' ;
                return $active;
            })
        ->addColumn('operations','<a href="#" url="{{ URL::route( \'employee.edit\', array( $id )) }}"class=\'btn btn-xs btn-info btn-edit\' ><i class=\'fa fa-edit\'></i>Edit</a> &nbsp; 
        	<a data="#" url="{{ URL::route( \'employee.destroy\', array( $id )) }} "class=\'btn btn-xs btn-danger btn-destroy\' id=\'{{$id}}\' _token={{csrf_token()}}><i class=\'fa fa-close\'></i>Delete</a>')
        ->make(true);
    }

}

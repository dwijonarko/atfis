<?php namespace App\Http\Controllers;

use App\Schedule;
use App\Employee;
use App\Attendance;

class HomeController extends Controller {


	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (\Auth::user()->hasRole('user')) {
			$employee = Employee::where('nik',\Auth::user()->username)->first(array('employees.id'));
			$schedules = Employee::find($employee->id)->schedule()->get();
			$attendances = Attendance::where('employee_id',$employee->id)->where('date',date('Y-m-d'))->take(5)->orderBy('time','desc')->get();
			return view('home',array('schedules'=>$schedules,'attendances'=>$attendances));
		}else{
			return view('home');
		}

	}

}

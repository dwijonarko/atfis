<?php
// use App\User;
// use App\Role;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// $router->get('/seed', function()
// {
  
//   $user = User::where('username', 'user')->first();

//   $role = Role::where('name','user')->first();
//   $user->assignRole($role);

// });
Route::get('/dailyAll',['middleware' => 'auth', 'uses'=>'ReportsController@dailyAll']);
Route::get('/monthlyAll',['middleware' => 'auth', 'uses'=>'ReportsController@monthlyAll']);
Route::get('/annualyAll',['middleware' => 'auth', 'uses'=>'ReportsController@annualyAll']);
Route::get('/daily',['middleware' => 'auth', 'uses'=>'ReportsController@daily']);
Route::get('/monthly',['middleware' => 'auth', 'uses'=>'ReportsController@monthly']);
Route::get('/annualy',['middleware' => 'auth', 'uses'=>'ReportsController@annualy']);
Route::get('/', ['middleware' => 'auth', 'uses'=>'HomeController@index','as'=>'root']);
Route::get('home', 'HomeController@index');
Route::get('position/getData', ['middleware' => 'auth', 'uses'=>'PositionController@getData']);
Route::get('office/getData', ['middleware' => 'auth', 'uses'=>'OfficeController@getData']);
Route::get('schedule/getData', ['middleware' => 'auth', 'uses'=>'ScheduleController@getData']);
Route::get('employee/getData', ['middleware' => 'auth', 'uses'=>'EmployeeController@getData']);
Route::resource('position', 'PositionController');
Route::resource('office', 'OfficeController');
Route::resource('schedule', 'ScheduleController');
Route::resource('employee', 'EmployeeController');
Route::resource('attendance', 'AttendanceController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

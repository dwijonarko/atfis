<table class="table table-consended">
        <tr>

          <th>No Id</th><td>{{$employee->id}}</td>
        </tr>
        <tr>
          <th>No Induk</th><td>{{$employee->nik}}</td>
        </tr>
        <tr>
          <th>Nama</th><td>{{$employee->name}}</td>
        </tr>
        <tr>
          <th>Tempat Lahir</th><td>{{$employee->place_of_birth}}</td>
        </tr>
        <tr>
          <th>Tanggal Lahir</th><td>{{date('d - M - Y',strtotime($employee->birthday))}}</td>
        </tr>
        <tr>
          <th>Alamat</th><td>{{$employee->address}}</td>
        </tr>
        <tr>
          <th>Telp</th><td>{{$employee->phone}}</td>
        </tr>
        <tr>
          <th>Email</th><td>{{$employee->email}}</td>
        </tr>
        <tr>
          <th>Jabatan</th><td>{{$employee->position->name}}</td>
        </tr>
        <tr>
          <th>Kantor</th><td>{{$employee->office->name}}</td>
        </tr>
        <tr>
          <th>Jadwal</th><td>{{$employee->schedule->code}}</td>
        </tr>
        <tr>
          <th>Status</th><td>{{ ($employee->active==1) ? 'Aktif':'Tidak Aktif' }}</td>
        </tr>
        </table>
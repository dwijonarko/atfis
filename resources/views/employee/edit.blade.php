<form action="{{URL::route('employee.update',array($employee->id))}}" class="form-horizontal" id="newForm">
<input name="_method" type="hidden" value="PUT">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <div class="form-group">
    <label for="nik" class="col-sm-2 control-label">Nomor Induk </label>
    <div class="col-sm-10">
      <input type="hidden" name="id" value="{{$employee->id}}">
      <input type="text" class="form-control" id="nik" placeholder="Nomor Induk Pegawai"  name="nik" value="{{$employee->nik}}" readonly="readonly">
    </div>
  </div>
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Nama </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" placeholder="Nama Pegawai"  name="name" value="{{$employee->name}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="place_of_birth" class="col-sm-2 control-label">Tempat Lahir </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="place_of_birth" placeholder="Tempat Lahir"  name="place_of_birth" value="{{$employee->place_of_birth}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="birthday" class="col-sm-2 control-label">Tanggal Lahir </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="birthday" placeholder="Tempat Lahir"  name="birthday" value="{{date('d:m:Y',strtotime($employee->birthday))}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="address" class="col-sm-2 control-label">Alamat </label>
    <div class="col-sm-10">
      <textarea name="address" id="address" cols="30" rows="5" class="form-control" required>{{$employee->address}}</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="phone" class="col-sm-2 control-label">Telepon </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="phone" placeholder="Nomor Telp"  name="phone" value="{{$employee->phone}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email </label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="email" placeholder="Email Pegawai"  name="email" value="{{$employee->email}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="position" class="col-sm-2 control-label">Jabatan</label>
    <div class="col-sm-10">
      <select name="position_id" id="position" class="form-control">
        <option value="0">--Pilih Jabatan--</option>
        @foreach ($positions as $position)
            <option value="{{$position->id}}" {{($position->id == $employee->position_id)? 'selected' : '' }}  >{{$position->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="office" class="col-sm-2 control-label">Kantor</label>
    <div class="col-sm-10">
      <select name="office_id" id="office" class="form-control">
        <option value="0">--Pilih Kantor--</option>
        @foreach ($offices as $office)
            <option value="{{$office->id}}" {{($office->id == $employee->office_id)? 'selected' : '' }} >{{$office->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="schedule" class="col-sm-2 control-label">Jadwal</label>
    <div class="col-sm-10">
      <select name="schedule_id" id="schedule" class="form-control">
        <option value="0">--Pilih Jadwal--</option>
        @foreach ($schedules as $schedule)
            <option value="{{$schedule->id}}" {{($schedule->id == $employee->schedule_id)? 'selected' : '' }} >{{$schedule->code}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="active" class="col-sm-2 control-label">Status</label>
    <div class="col-sm-10">
      <label class="checkbox-inline">
        <input type="checkbox" id="active" name="active" value="1" {{($employee->active=='1')? 'checked' : '' }}> Aktif
      </label>
    </div>
  </div>
</form>
<script>
  $('#birthday').datetimepicker({locale:'id',format: 'DD:MM:YYYY'});

</script>
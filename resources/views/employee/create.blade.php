<form action="{{URL::to('employee')}}" class="form-horizontal" id="newForm">
<input name="_method" type="hidden" value="POST">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <div class="form-group">
    <label for="nik" class="col-sm-2 control-label">Nomor Induk </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nik" placeholder="Nomor Induk Pegawai"  name="nik" required>
    </div>
  </div>
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Nama </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" placeholder="Nama Pegawai"  name="name" required>
    </div>
  </div>
  <div class="form-group">
    <label for="place_of_birth" class="col-sm-2 control-label">Tempat Lahir </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="place_of_birth" placeholder="Tempat Lahir"  name="place_of_birth" required>
    </div>
  </div>
  <div class="form-group">
    <label for="birthday" class="col-sm-2 control-label">Tanggal Lahir </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="birthday" placeholder="Tanggal Lahir"  name="birthday" required>
    </div>
  </div>
  <div class="form-group">
    <label for="address" class="col-sm-2 control-label">Alamat </label>
    <div class="col-sm-10">
      <textarea name="address" id="address" cols="30" rows="5" class="form-control" required></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="phone" class="col-sm-2 control-label">Telepon </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="phone" placeholder="Nomor Telp"  name="phone" required>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email </label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="email" placeholder="Email Pegawai"  name="email" required>
    </div>
  </div>
  <div class="form-group">
    <label for="position" class="col-sm-2 control-label">Jabatan</label>
    <div class="col-sm-10">
      <select name="position_id" id="position" class="form-control">
        <option value="0">--Pilih Jabatan--</option>
        @foreach ($positions as $position)
            <option value="{{$position->id}}">{{$position->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="office" class="col-sm-2 control-label">Kantor</label>
    <div class="col-sm-10">
      <select name="office_id" id="office" class="form-control">
        <option value="0">--Pilih Kantor--</option>
        @foreach ($offices as $office)
            <option value="{{$office->id}}">{{$office->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="schedule" class="col-sm-2 control-label">Jadwal</label>
    <div class="col-sm-10">
      <select name="schedule_id" id="schedule" class="form-control">
        <option value="0">--Pilih Jadwal--</option>
        @foreach ($schedules as $schedule)
            <option value="{{$schedule->id}}">{{$schedule->code}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="active" class="col-sm-2 control-label">Status</label>
    <div class="col-sm-10">
      <label class="checkbox-inline">
        <input type="checkbox" id="active" name="active" value="1"> Aktif
      </label>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-12">
      <label class="checkbox-inline">
        <input type="checkbox" id="register" name="register" value="1"> Daftarkan untuk login di web ATFIS?<sup>*</sup>
      </label><br>
      <span class='label label-warning'><sup>*</sup>Login pengguna dan password menggunakan NIK</span>
    </div>
  </div>
</form>
<script>
  $('#birthday').datetimepicker({locale:'id',format: 'DD:MM:YYYY'});

</script>
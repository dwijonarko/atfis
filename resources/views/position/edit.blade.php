<form action="{{ URL::route( 'position.update', array( $position->id )) }}"  class="form-horizontal" id="newForm">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input name="_method" type="hidden" value="PUT">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-10">
      <input type="hidden" name="id" value="{{$position->id}}">
      <input type="text" class="form-control" id="name" placeholder="Nama Jabatan"  name="name" value="{{$position->name}}" required>
    </div>
  </div>
</form>
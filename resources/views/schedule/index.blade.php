@extends('app')

@section('content')
  <div class="row">
      <h2 class="page-header">Master Data <small>- Jadwal</small></h2>
      <p><a href="#" class="btn btn-sm btn-primary" id="btn-new">Tambah Jadwal</a></p>
  </div>
  {{-- Datatable --}}
  <table id="users-table" class="table table-condensed">
    <thead>
    <tr>
        <th>Id</th>
        <th>Kode</th>
        <th>Hari</th>
        <th>Jam Masuk</th>
        <th>Jam Keluar</th>
        <th>Operation</th>
    </tr>
    </thead>
  </table>
  
  {{-- Modal Form --}}
  <div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Master Jabatan</h4>
            </div>
            <div class="modal-body"></div>
            <div id="response"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save">Save changes</button>
            </div>
        </div>
    </div>
  </div>

  {{-- Delete Form --}}
  <div class="modal" id="deleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Master Jadwal</h4>
            </div>
            <div class="modal-body">
              <h4>Are you sure ?</h4>
            </div>
            <div id="response"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="delete">Delete</button>
            </div>
        </div>
    </div>
  </div>

@endsection

@section('javascript')
  <script>
  var table = $('#users-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ url("schedule/getData") }}',
      columns: [
          {data: 'id', name: 'id'},
          {data: 'code', name: 'code'},
          {data: 'day', name: 'day'},
          {data: 'in_time', name: 'in_time'},
          {data: 'out_time', name: 'out_time'},
          {data: 'operations', name: 'operations'},
      ]
  });
  table.ajax.reload();
</script>
@endsection

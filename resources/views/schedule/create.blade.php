<form action="{{URL::to('schedule')}}" class="form-horizontal" id="newForm">
<input name="_method" type="hidden" value="POST">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <div class="form-group">
    <label for="code" class="col-sm-2 control-label">Kode </label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="code" placeholder="Kode Jadwal"  name="code" required>
    </div>
  </div>
  <div class="form-group">
    <label for="day" class="col-sm-2 control-label">Hari </label>
    <div class="col-sm-10">
      <select name="day" id="day" class="form-control">
        <option value="1">Minggu</option>
        <option value="2">Senin</option>
        <option value="3">Selasa</option>
        <option value="4">Rabu</option>
        <option value="5">Kamis</option>
        <option value="6">Jumat</option>
        <option value="7">Sabtu</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="in_time" class="col-sm-2 control-label" >Waktu Masuk</label>
    <div class="col-sm-10">
      <input type="text" class="form-control timepicker" id="in_time" placeholder="JJ:MM:DD" name="in_time" required>
    </div>
  </div>
  <div class="form-group">
    <label for="out_time" class="col-sm-2 control-label">Waktu Keluar</label>
    <div class="col-sm-10">
      <input type="text" class="form-control timepicker" id="out_time" name="out_time" placeholder="JJ:MM:DD" required>
    </div>
  </div>
</form>
<script>
  $('#in_time').val('00:00:00');
  $('#out_time').val( moment().format("00:00:00"));
  $('#in_time').datetimepicker({locale:'id',format: 'HH:mm:ss'});
  $('#out_time').datetimepicker({locale:'id',format: 'HH:mm:ss'});

</script>
<form action="{{URL::route('schedule.update',array($schedule->id))}}" class="form-horizontal" id="newForm">
<input name="_method" type="hidden" value="PUT">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <div class="form-group">
    <label for="code" class="col-sm-2 control-label">Kode </label>
    <div class="col-sm-10">
          <input type="hidden" name="id" value="{{$schedule->id}}">
      <input type="text" class="form-control" id="code" placeholder="Kode Jadwal"  name="code" value="{{$schedule->code}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="day" class="col-sm-2 control-label">Hari </label>
    <div class="col-sm-10">
      <select name="day" id="day" class="form-control">
        <option value="1" {{($schedule->day == 1)? 'selected' : '' }} >Minggu</option>
        <option value="2" {{($schedule->day == 2)? 'selected' : '' }}>Senin</option>
        <option value="3" {{($schedule->day == 3)? 'selected' : '' }}>Selasa</option>
        <option value="4" {{($schedule->day == 4)? 'selected' : '' }}>Rabu</option>
        <option value="5" {{($schedule->day == 5)? 'selected' : '' }}>Kamis</option>
        <option value="6" {{($schedule->day == 6)? 'selected' : '' }}>Jumat</option>
        <option value="7" {{($schedule->day == 7)? 'selected' : '' }}>Sabtu</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="in_time" class="col-sm-2 control-label" >Waktu Masuk</label>
    <div class="col-sm-10">
      <input type="text" class="form-control timepicker" id="in_time" placeholder="JJ:MM:DD" name="in_time" value="{{$schedule->in_time}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="out_time" class="col-sm-2 control-label">Waktu Keluar</label>
    <div class="col-sm-10">
      <input type="text" class="form-control timepicker" id="out_time" name="out_time" placeholder="JJ:MM:DD" value="{{$schedule->out_time}}" required>
    </div>
  </div>
</form>
<script>
  $('#in_time').datetimepicker({locale:'id',format: 'HH:mm:ss'});
  $('#out_time').datetimepicker({locale:'id',format: 'HH:mm:ss'});
</script>
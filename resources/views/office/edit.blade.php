<form action="{{URL::route('office.update', array($office->id))}}" class="form-horizontal" id="newForm">
<input name="_method" type="hidden" value="PUT">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-10">
    <input type="hidden" name="id" value="{{$office->id}}">
      <input type="text" class="form-control" id="name" placeholder="Nama Kantor"  name="name" value="{{$office->name}}" required>
    </div>
  </div>
  <div class="form-group">
    <label for="address" class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-10">
      <textarea name="address" id="address" cols="30" rows="10" class="form-control">{{$office->address}}</textarea>
    </div>

  </div>
  <div class="form-group">
    <label for="phone" class="col-sm-2 control-label">Telepon</label>
    <div class="col-sm-10">
      <input type="phone" class="form-control" id="phone" placeholder="Telepon"  name="phone" value="{{$office->phone}}" required>
    </div>
  </div>
</form>
@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">Home</div>

				<div class="panel-body">

				<?php header("refresh:60;url=/") ?>
					You are logged in! {{ Auth::user()->realname}} 
					@if (Auth::user()->hasRole('user'))
					<hr>
						@if(count($attendances) ==0)
							<p class="alert alert-info info">Anda belum mengisi presensi masuk hari ini</p>
						@else
							<?php $i=0;
							foreach($attendances as $attendance)
							{
								$i += $attendance->in_out;
							}
						  ?>
							@if($i==0)
							<p class="alert alert-warning info">Anda belum mengisi presensi pulang hari ini</p>
							@endif
						@endif
						<p>Klik tombol untuk mengisi daftar kehadiran 
						<a href="#" url="{{ URL::route( 'attendance.store') }}" id="0" _token={{csrf_token()}} class="btn btn-info btn-sm btn-post"> <i class='fa fa-check'></i> Masuk</a> 
						<a href="#" url="{{ URL::route( 'attendance.store') }}" id="1" _token={{csrf_token()}} class="btn btn-warning btn-sm btn-post"> <i class='fa fa-check'></i> Pulang</a> </p>

						{{-- Attendance Form --}}
					  <div class="modal" id="attendanceModal">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					                <h4 class="modal-title">Daftar Hadir</h4>
					            </div>
					            <div class="modal-body">
					              <h4>Isi Presensi? </h4>
					            </div>
					            <div id="response"></div>
					            <div class="modal-footer">
					                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					                <button type="button" class="btn btn-primary" id="save">Save</button>
					            </div>
					        </div>
					    </div>
					  </div>
					@endif
				</div>
			</div>
		</div>
	</div>
	@if (Auth::user()->hasRole('user'))
	<div class="row">
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">Jadwal</div>
				<div class="panel-body">
				<table class="table table-hover">
					<tr>
						<th>No</th>
						<th>Hari</th>
						<th>Masuk</th>
						<th>Pulang</th>
					</tr>
				@foreach ($schedules as $schedule)
					<tr>
						<td>1</td>
						<td>Senin</td>
						<td>{{$schedule->in_time}}</td>
						<td>{{$schedule->out_time}}</td>
					</tr>
										<tr>
						<td>2</td>
						<td>Selasa</td>
						<td>{{$schedule->in_time}}</td>
						<td>{{$schedule->out_time}}</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Rabu</td>
						<td>{{$schedule->in_time}}</td>
						<td>{{$schedule->out_time}}</td>
					</tr>
					<tr>
						<td>4</td>
						<td>Kamis</td>
						<td>{{$schedule->in_time}}</td>
						<td>{{$schedule->out_time}}</td>
					</tr>
					<tr>
						<td>5</td>
						<td>Jumat</td>
						<td>{{$schedule->in_time}}</td>
						<td>{{$schedule->out_time}}</td>
					</tr>

				@endforeach
				</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">Log Absensi</div>
				<div class="panel-body">
				<table class="table table-hover" >
					<tr>
						<th>No</th>
						<th>Tanggal</th>
						<th>Jam</th>
						<th>Keterangan</th>
					</tr>
					<tbody id="log">
					<?php $i = 1 ?>
					@foreach ($attendances as $attendance)
					<tr>
						<td>{{$i}}</td>
						<td>{{date('d-m-Y',strtotime($attendance->date))}}</td>
						<td>{{$attendance->time}}</td>
						<td>{{ $attendance->in_out =='0' ? 'Masuk' : 'Pulang' }}</td>
					</tr>
					<?php $i++ ?>
					@endforeach
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
	@endif
</div>
@endsection


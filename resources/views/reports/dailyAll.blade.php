@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 well" >
			<div class="col-md-12 text-center"><h2>Laporan Kehadiran Karyawan Harian</h2></div>
			<table><tr><td>Tanggal : </td><td> <strong> {{date('d-M-Y')}} </strong></td></tr></table>
			<br>
			<table class="table table-condensed ">
				<tr>
					<th>No</th>
					<th>Nomor Induk Karyawan</th>
					<th>Tanggal Kehadiran</th>
					<th>Masuk</th>
					<th>Keluar</th>
				</tr>
				<tbody>
				<?php $i = 1 ?>
				@foreach ($reports as $report)
				<tr>
					<td>{{$i}}</td>
					<td>{{$report->nik}}</td>
					<td>{{date('d - M - Y',strtotime($report->date))}}</td>
					<td>{{$report->masuk}}</td>
					<td>{{$report->keluar}}</td>
				</tr>
				<?php $i++ ?>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>	
</div>
@endsection


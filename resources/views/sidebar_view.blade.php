<ul class="metismenu sidebar-nav" id="sidebar">

  @if (Auth::user()->hasRole('administrator'))
  <li>
    <a href="#">Master<span class="sub_icon fa fa-file"></span></a>
    <ul class="sidebar-sub-nav">
      <li><a href="employee" class="link_active">Pegawai<span class="sub_icon fa fa-users"></span></a></li>
      <li><a href="position">Jabatan<span class="sub_icon fa fa-briefcase"></span></a></li>
      <li><a href="office">Kantor<span class="sub_icon fa fa-building-o"></span></a></li>
      <li><a href="schedule">Jadwal<span class="sub_icon fa fa-calendar"></span></a></li>
    </ul>
  </li>
 <li>
    <a href="#">Laporan<span class="sub_icon fa fa-bar-chart"></span></a>
    <ul class="sidebar-sub-nav">
      <li><a href="dailyAll">Harian<span class="sub_icon fa fa-calendar"></span></a></li>
      <li><a href="monthlyAll">Bulanan<span class="sub_icon fa fa-calendar"></span></a></li>
      <li><a href="annualyAll">Tahunan<span class="sub_icon fa fa-calendar"></span></a></li>
    </ul>
  </li>
    @elseif (Auth::user()->hasRole('user'))
  <li>
    <a href="#">Laporan<span class="sub_icon fa fa-bar-chart"></span></a>
    <ul class="sidebar-sub-nav">
      <li><a href="daily">Harian<span class="sub_icon fa fa-calendar"></span></a></li>
      <li><a href="monthly">Bulanan<span class="sub_icon fa fa-calendar"></span></a></li>
      <li><a href="annualy">Tahunan<span class="sub_icon fa fa-calendar"></span></a></li>
    </ul>
  </li> 
    @endif
   

</ul>
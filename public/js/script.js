$(document).ready(function(){
  $("#sidebar").metisMenu();
  $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("active");
  });
  $.material.init();
  $('#datatable').dataTable({
    "bPaginate": true,
  });

  var path =  window.location.origin;
  var url = location.pathname.split("/")[1];
  if (location.pathname.split("/")[1]!='' ) {  
      $('a[href^="'+ url + '"]').addClass('btn-primary');
      $('a[href^="'+ url + '"]').parent().parent().addClass('collapse in');
  }


  $('#btn-new').click(function(){
    $('.modal-body').html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\" style=\"width: 100%\"></div></div>");
    $('#myModal').modal({show:true});
        setTimeout(function(){
          $('.modal-body').load(url+'/create');
        },500);
  });

   $('body').on('click', '.btn-edit', function() { 
    //alert('edit');
    var edit_url = $(this).attr('url');
    $('.modal-body').html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\" style=\"width: 100%\"></div></div>");
    $('#myModal').modal({show:true});
        setTimeout(function(){
          $('.modal-body').load(edit_url);
        },500);
  });

  $('body').on('click', '.btn-show', function() { 
    var show_url = $(this).attr('url');
    $('.modal-body').html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\" style=\"width: 100%\"></div></div>");
    $('#showModal').modal({show:true});
        setTimeout(function(){
          $('.modal-body').load(show_url);
        },500);
  });

  $('body').on('click', '.btn-destroy', function() { 
    //alert('delete');
    var id =  $(this).attr('id');
    var _token = $(this).attr('_token');
    var url=  $(this).attr('url');
    $('.modal-body').html("Are you sure?");
    $('#deleteModal').modal({show:true})
        .one('click', '#delete', function(){
          $('.modal-body').html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\" style=\"width: 100%\"></div></div>");
            setTimeout(function(){
            $.ajax({
            type: "DELETE",
            url: url,
            data: {"id":id,"_token": _token,},
            async:true,
            success: function(response) {
              var json_obj = $.parseJSON(response);
              var output = "<div class='alert alert-success'>"+json_obj[0]['message']+"</div>"; 
              $(".modal-body").html(output);
              setTimeout(function() {
                $('#delete').removeAttr('disabled');
                table.ajax.reload();
                setTimeout(function(){
                  $('#deleteModal').modal('hide');
                }),3000;
              }, 2000);
            },
            error: function(msg)
            {
              $('.modal-body').html('<p class=\'alert alert-danger\'>Cannot delete record, check the relationship</p>');
            }
            });
          }, 1000);
        });
  });

$('body').on('click', '.btn-post', function() { 
    //alert('Check log');
    var id =  $(this).attr('id');
    var _token = $(this).attr('_token');
    var url=  $(this).attr('url');
    $('.modal-body').html("Isi Presensi?");
    $('#attendanceModal').modal({show:true})
        .one('click', '#save', function(){
//          $('.modal-body').html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\" style=\"width: 100%\"></div></div>");
            setTimeout(function(){
            $.ajax({
            type: "POST",
            url: url,
            data: {"id":id,"_token": _token,},
            async:true,
            success: function(response) {
              var json_obj = $.parseJSON(response);
              var output = "<div class='alert alert-success'>"+json_obj[0]['message']+"</div>"; 
              $(".modal-body").html(output);
              var attendances = json_obj[0]['attendances'] ;
              var attendance;
              attendances.forEach(function (element, index, array) {
                attendance += ("<tr><td>"+(index+1)+"</td><td>"+moment(element['date']).format('DD-MM-YYYY')+"</td><td>"+element['time']+"</td><td>"+(element['in_out']== '0' ? 'Masuk' : 'Pulang' )+"</td></tr>");
              });
              $('#log').hide().html(attendance).fadeIn(1500);
              $('.info').remove();

              setTimeout(function() {
                $('#save').removeAttr('disabled');
                setTimeout(function(){
                  $('#attendanceModal').modal('hide');
                }),1500;
              }, 1000);
            },
            error: function(msg)
            {
              console.log(msg);
            },
            complete: function(msg)
            {
              $('#response').html('');
            }
            });
          }, 500);
        });
  });

  $('body').on('click', '#save', function() { 
    $('#save').attr('disabled','disabled');
    $('#response').html("<div class=\"progress progress-striped active\"><div class=\"progress-bar\" style=\"width: 100%\"></div></div>");
    var url=$('#newForm').attr('action');
    var type=$("input[name=_method]").val()
    var form_data = $("#newForm").serialize();
    setTimeout(function(){
      $.ajax({
        type: type,
        url: url,
        data: form_data,
        async:true,
        success: function(response) {
        var json_obj = $.parseJSON(response);
        if (typeof json_obj[0] !== 'undefined') {
          var output = "<div class='alert alert-success'>"+json_obj[0]['message']+"</div>"; 
          $(":input").each(function(){
            this.value = "";          
          });
          setTimeout(function() {
            $("#response").html("");
            $('#save').removeAttr('disabled');
            setTimeout(function(){
              $('#myModal').modal('hide');
            }),3000;
          }, 2000);

        } else{
          var output="<ul class='alert alert-danger list-unstyled'>";
          $.each(json_obj, function(i,val) {
            for ( var j in val) {
                output +="<li >" + val[j]+"</li>";        
            }
          });
          output+="</ul>";
          $('#save').removeAttr('disabled');
        };
        $('#response').html(output);
        table.ajax.reload();
      },
        error: function(msg){
          console.log(msg);
        },
        complete: function(msg){
          $('#loading').html('   ');
        }
      });
    }, 500);
  });
});


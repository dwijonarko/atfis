var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'jquery': './vendor/bower_components/jquery/',
    'bootstrap': './vendor/bower_components/bootstrap/dist/',
    'bootstrapmaterial': './vendor/bower_components/bootstrap-material-design/dist/',
    'datatables': './vendor/bower_components/datatables/media/',
    'eonasdanbootstrapdatetimepicker': './vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/',
    'fontawesome':'./vendor/bower_components/font-awesome/',
    'metismenu': './vendor/bower_components/metisMenu/dist/',
    'moment':'./vendor/bower_components/moment/min/',
}

elixir(function(mix) {
    mix.styles([
            paths.bootstrap + "css/bootstrap.min.css",
            paths.bootstrapmaterial + "css/material.min.css",
            paths.bootstrapmaterial + "css/material-fullpalette.min.css",
            paths.bootstrapmaterial + "css/roboto.min.css",
            paths.bootstrapmaterial + "css/ripples.min.css",
            paths.fontawesome + "css/font-awesome.min.css",
            paths.datatables + "css/jquery.dataTables.min.css",
            paths.metismenu + "metisMenu.min.css",
            paths.eonasdanbootstrapdatetimepicker + "css/bootstrap-datetimepicker.min.css",
        ],'public/css/app.css','./')
        .copy(paths.bootstrap + 'fonts/**', 'public/fonts')
        .copy(paths.bootstrapmaterial + 'fonts/**', 'public/fonts')
        .copy(paths.fontawesome + 'fonts/**', 'public/fonts')
        .copy(paths.datatables + 'images/**', 'public/images')
        .scripts([
            paths.jquery + "dist/jquery.min.js",
            paths.bootstrap + "js/bootstrap.min.js",
            paths.bootstrapmaterial + "js/material.min.js",
            paths.bootstrapmaterial + "js/ripples.min.js",
            paths.datatables + "js/jquery.dataTables.min.js",
            paths.metismenu + "metisMenu.min.js",
            paths.moment + "moment.min.js",
            paths.moment + "moment-with-locales.min.js",
            paths.eonasdanbootstrapdatetimepicker + "js/bootstrap-datetimepicker.min.js"
        ],'public/js/app.js', './');
});